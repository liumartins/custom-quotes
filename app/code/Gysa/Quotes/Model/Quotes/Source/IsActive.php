<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 22/02/19
 * Time: 09:35
 */

namespace Gysa\Quotes\Model\Quotes\Source;




class IsActive implements \Magento\Framework\Data\OptionSourceInterface
{
    protected $quotes;

    public function __construct(\Magento\Quote\Model\Quote $quotes)
    {
        $this->quotes = $quotes;
    }

    public function toOptionArray()
    {
        // TODO: Implement toOptionArray() method.
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->quotes->getAvailableStatuses();
        foreach ($availableOptions as $key => $value)
        {
            $options[] = [
              'label' => $value,
              'value' => $key,
            ];
        }

        return $options;
    }


}