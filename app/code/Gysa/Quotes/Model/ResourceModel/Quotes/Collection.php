<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 15/03/19
 * Time: 15:48
 */

namespace Gysa\Quotes\Model\ResourceModel\Quotes;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\VersionControl\Collection
{
    /**
     * Resource initialization
     *
     * @return void
     */

    protected $_idFieldName = "entity_id";
    protected function _construct()
    {
        $this->_init(\Magento\Quote\Model\Quote::class, \Magento\Quote\Model\ResourceModel\Quote::class);
    }
}