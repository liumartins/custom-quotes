<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 26/02/19
 * Time: 16:30
 */

namespace Gysa\Quotes\Controller\Quotes;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\ResponseFactory;
use Magento\Quote\Model\QuoteFactory;
use Magento\Checkout\Model\Session;



class Cart extends \Magento\Framework\App\Action\Action
{
    protected $resultFactory;
    protected $responseFactory;
    protected $quoteFactory;
    protected $_checkoutSession;

    public function __construct(ResultFactory $resultFactory, Context $context, ResponseFactory $responseFactory, QuoteFactory $quoteFactory, Session $_checkoutSession)
    {
        parent::__construct($context);
        $this->resultFactory = $resultFactory;
        $this->responseFactory = $responseFactory;
        $this->quoteFactory = $quoteFactory;
        $this->_checkoutSession = $_checkoutSession;
    }

    public function execute()
    {
        /* Load quote id */
        $quoteId = $this->getRequest()->getParam('quoteId');

        /* Load in checkout session as guest */

        $this->_checkoutSession->setQuoteId($quoteId);
        /* Redirect to cart page */
        //$this->responseFactory->create()->setRedirect('/checkout/cart/index')->sendResponse();
        $this->_redirect('checkout/cart/index');

    }
}