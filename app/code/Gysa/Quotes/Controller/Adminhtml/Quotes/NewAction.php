<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 20/02/19
 * Time: 15:55
 */

namespace Gysa\Quotes\Controller\Adminhtml\Quotes;


class NewAction extends \Magento\Backend\App\Action
{

    public function execute()
    {
        $this->_forward('edit');
    }

}