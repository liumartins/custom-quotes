<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 07/03/19
 * Time: 15:32
 */

namespace Gysa\Quotes\Controller\Adminhtml\Quotes;

use Magento\Backend\App\Action\Context;
use Magento\Quote\Model\QuoteFactory;

class Delete extends \Magento\Backend\App\Action
{
    protected $quote;
    
    public function __construct(
        Context $context,
        QuoteFactory $quoteFactory
    ) {
        parent::__construct($context);
        $this->quote = $quoteFactory;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed(){
        return $this->_authorization->isAllowed('Gysa_Quotes::delete');
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('entity_id');
        $model = $this->quote->create();

        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('Quote deleted'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['entity_id' => $id]);
            }
        }
        $this->messageManager->addError(__('Quote does not exist'));
        return $resultRedirect->setPath('*/*/');
    }
}