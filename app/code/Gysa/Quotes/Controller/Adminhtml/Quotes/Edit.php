<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 21/02/19
 * Time: 11:00
 */

namespace Gysa\Quotes\Controller\Adminhtml\Quotes;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Quote\Model\QuoteFactory;
use Psr\Log\LoggerInterface;


class Edit extends \Magento\Backend\App\Action
{
    protected $_coreRegistry = null;
    protected $resultPageFactory = false;
    protected $quoteFactory;
    protected $log;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        QuoteFactory $quoteFactory,
        Registry $registry,
        LoggerInterface $logger

    )
    {

        $this->resultPageFactory = $resultPageFactory;
        $this->quoteFactory = $quoteFactory;
        $this->_coreRegistry = $registry;
        $this->log = $logger;
        parent::__construct($context);

    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Gysa_Quotes::quotes_save');
    }


    public function execute()
    {
        $id = $this->getRequest()->getParam('entity_id');
        $quote = $this->quoteFactory->create();

        if($id)
        {
            $quote->load($id);
            if(!$quote->getId())
            {
                $this->messageManager->addError(__('This Quote no longer exist'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $data = $this->_session->getData('quotes_quotes_data', true);
        if(!empty($data))
        {
            $quote->setData($data);
        }


        $resultPage = $this->resultPageFactory->create();

        $resultPage->getConfig()->getTitle()
            ->set(__('Quotes'))
            ->prepend($id ? _("Edit Quote: ") . $id : __('New Quote'));

        return $resultPage;

    }
}