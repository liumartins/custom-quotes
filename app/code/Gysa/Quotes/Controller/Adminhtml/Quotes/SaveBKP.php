<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 27/02/19
 * Time: 09:20
 */

namespace Gysa\Quotes\Controller\Adminhtml\Quotes;

use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;
use Magento\Quote\Model\QuoteFactory;

class Save extends \Magento\Backend\App\Action
{
    protected $quoteFactory;

    /**
     * @param Action\Context $context
     */
    public function __construct(Action\Context $context, QuoteFactory $quoteFactory)
    {
        $this->quoteFactory = $quoteFactory;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Gysa_Quotes::save');
    }

    public function execute(){
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($data){

            $model = $this->quoteFactory->create();
            $id = $this->getRequest()->getParam('entity_id');

            if ($id) {
                $model->load($id);
            }

            $model->setData($data);

            $this->_eventManager->dispatch(
                'gysa_quote_prepare_save',
                ['quote' => $model, 'request' => $this->getRequest()]
            );

            try {

                $model->save();
                $this->messageManager->addSuccess(__('You saved this Post.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['entity_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the post.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('entity_id')]);
        }
        return $resultRedirect->setPath('*/*/');


    }
}
