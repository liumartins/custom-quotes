<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 27/02/19
 * Time: 09:20
 */

namespace Gysa\Quotes\Controller\Adminhtml\Quotes;

use Gysa\Quotes\Block\Adminhtml\Quotes\Edit\Products;
use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;
use Magento\Quote\Model\QuoteFactory;
use Magento\Quote\Model\QuoteRepository;
use Magento\Catalog\Model\ProductFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Backend\Helper\Js;


class Save extends \Magento\Backend\App\Action
{
    protected $quoteFactory;
    protected $product;
    protected $_storeManager;
    protected $_jsHelper;
    protected $_quoteRepository;

    /**
     * @param Action\Context $context
     */
    public function __construct(Action\Context $context, QuoteFactory $quoteFactory, ProductFactory $productFactory, StoreManagerInterface $storeManager, Js $jsHelper, QuoteRepository $quoteRepository)
    {
        $this->quoteFactory = $quoteFactory;
        $this->product = $productFactory;
        $this->_storeManager = $storeManager;
        $this->_jsHelper = $jsHelper;
        $this->_quoteRepository = $quoteRepository;

        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Gysa_Quotes::save');
    }


    public function execute()
    {


        $id = $this->getRequest()->getParam('quote_id');
        $store = $this->_storeManager->getStore();
        $urlBase = $store->getBaseUrl();
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();

        if(empty($data['products_id'])){ //Verify if the user has set a products on cart.
            return  $this->validateCart();
        }

        $items = $this->prepareItems($data['products_id'], $data['qty'], $data['item_id']);
        var_dump($items);

        if($items == null){

            $preivousUrl = $this->_redirect->getRefererUrl();
            $this->messageManager->addError("The quantity of products cannot be empty");
            return $resultRedirect->setPath('*/*/*/');

        } else {
        if ($data) {
            $model = $this->quoteFactory->create();
            if (!empty($id)) {
                $model->load($id);
            }

            try {
                    $model->setStore($store);
                    $model->setCurrency();
                    $model->setCustomerEmail($data['customer_email']);
                    $model->setCustomerFirstname($data['customer_firstname']);

                    if ($data["is_active"] == "1") {
                        $model->setIsActive(true);
                    }
                    if(!empty($id)){
                        foreach($items as $item ){
                            $params = array(
                                'product' => $item['product_id'],
                                'qty'     => $item['product_qty'],
                            );

                           $tableName = $model->getCollection()->getTable('quote_item');
                           $sql = 'UPDATE '.$tableName. ' SET qty="'.$item['product_qty'].'" WHERE item_id="' .$item['item_id']. '"';

                           $model->getCollection()->getConnection()->query($sql);
                        }
                    }
                    else {
                        foreach ($items as $item) {
                            $_product = $this->product->create()->load($item['product_id']);
                            $model->addProduct($_product, intval($item['product_qty']));
                        }
                    }

                    $model->setIsMultiShipping(false);
                    $model->getBillingAddress();
                    $model->getShippingAddress()->setCollectShippingRates(true);
                    if (!empty($id)) {
                        $nextId = $id;
                    } else {
                        $lastId = $model->getCollection()->getLastItem()->getData('entity_id');
                        if ($lastId == '0') {
                            $lastId = "1";
                        }
                        $nextId = $lastId + 1;
                    }

                    $quoteUrl = $urlBase . 'gysaquotes/quotes/cart/quoteId/' . $nextId;

                    $model->setQuoteUrl($quoteUrl);
                    $model->setQuoteSource('Backend');
                    $model->collectTotals()->save();
                    $this->messageManager->addSuccess(__('You saved this Post.'));
                    return $resultRedirect->setPath('*/*/');

            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e);
                return $resultRedirect->setPath('*/*/');
            }
        }
        }
    }


    private function prepareItems($product_ids, $product_qtys, $item_ids){
        $error = "0";
        $item_ids = array_filter($item_ids, 'strlen'); //removes null values but leaves "0"
        $item_ids = array_filter($item_ids);
        $product_qtys = array_filter($product_qtys, 'strlen'); //removes null values but leaves "0"
        $product_qtys = array_filter($product_qtys);

        $id = $this->getRequest()->getParam('quote_id');
        $items = array();
        $size = count($product_ids);

        if(!empty($id)){
           array_unshift($product_qtys,"on"); //Deixas o array de quantidade com o mesmo tamanho do de Ids
           array_unshift($item_ids,"on"); //Deixas o array de quantidade com o mesmo tamanho do de Ids
           for ($i=1; $i<$size; $i++){
               if(empty($product_qtys[$i])){
                   $error = '1';
               } else {
                   $items[] = [
                       'item_id' => $item_ids[$i],
                       'product_id' => $product_ids[$i],
                       'product_qty' => $product_qtys[$i]
                   ];
               }
           }
        } else {
            $product_qtys = array_values($product_qtys);
            for ($i=0; $i<$size; $i++){
                if(empty($product_qtys[$i])){
                    $error = '1';
                } else {
                    $items[] =    [
                        'product_id' => $product_ids[$i],
                        'product_qty' => $product_qtys[$i]
                    ];
                }
            }
        }


            return $items;


    }

    private function validateCart(){
        $resultRedirect = $this->resultRedirectFactory->create();
        $preivousUrl = $this->_redirect->getRefererUrl();
        $this->messageManager->addError("Cart  is empty. It's necessary to add products on it");
        return $resultRedirect->setPath($preivousUrl);
    }
}

