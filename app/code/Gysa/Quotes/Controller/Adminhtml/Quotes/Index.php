<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 19/02/19
 * Time: 17:58
 */

namespace Gysa\Quotes\Controller\Adminhtml\Quotes;

class Index extends \Magento\Backend\App\Action
{
    protected $resultPageFactory = false;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;

    }


    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend((__('Quotes')));

        return $resultPage;
    }

    protected function initPage($resultPage)
    {
        $resultPage->setActiveMenu('Gysa_Quotes::quotes')
            ->addBreadcrumb(__('Manage Quote'), __('Manage Quote'))
            ->addBreadcrumb(__('Manage Quote'), __('Manage Quote'));

        return $resultPage;
    }


    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('gysa_quotes::quotes');
    }

    /*
     *
     public function execute()
    {
        die('test admin view');
    }
    */
}
