<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 12/03/19
 * Time: 10:43
 */

namespace Gysa\Quotes\Ui\Component\Listing\Column;


use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\Listing\Columns\Column;

class IsActive extends Column
{
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$items) {
                // $items['instock'] is column name
                if ($items['is_active'] == 1) {
                    $items['is_active'] = __('Enabled');
                } else {
                    $items['is_active'] = __("Disabled");
                }
            }
        }
        return $dataSource;
    }
}