<?php


namespace Gysa\Quotes\Setup;


use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;


class InstallData implements InstallDataInterface
{
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context){

        $setup->startSetup();

        $quote = 'quote';

        $setup->getConnection()
            ->addColumn(
                $setup->getTable($quote),
                'quote_source',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 25,
                    'comment' =>'Quote Source'
                ]
            );
        $setup->getConnection()
            ->addColumn(
                $setup->getTable($quote),
                'quote_url',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' =>'Quote Url'
                ]
            );
        $setup->endSetup();
    }
}