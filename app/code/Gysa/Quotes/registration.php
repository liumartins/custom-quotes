<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 19/02/19
 * Time: 17:24
 */

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Gysa_Quotes', __DIR__);
