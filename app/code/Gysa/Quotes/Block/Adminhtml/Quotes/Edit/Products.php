<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 26/02/19
 * Time: 13:45
 */

namespace Gysa\Quotes\Block\Adminhtml\Quotes\Edit;

use Magento\Backend\Block\Template\Context;
use Magento\Quote\Model\QuoteFactory;


class Products extends \Magento\Backend\Block\Widget
{
    protected $quoteFactory;

    public function __construct(
        Context $context,
        QuoteFactory $quoteFactory
    )
    {

        $this->quoteFactory = $quoteFactory;
        parent::__construct($context);
    }

    public function createGrid()
    {
        $id = $this->getRequest()->getParam('entity_id');
        $quote = $this->quoteFactory->create()->load($id);

        $items = $quote->getAllItems();

        return $items;
    }

    public function genURL(){
        $id = $this->getRequest()->getParam('entity_id');
        $q = $this->quoteFactory->create()->load($id);
        return $this->responseFactory->create()->setRedirect('/checkout/cart/index')->sendResponse();

    }
}