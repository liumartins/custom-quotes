<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 07/03/19
 * Time: 11:17
 */

namespace Gysa\Quotes\Block\Adminhtml\Quotes\Edit\Tab;


use Magento\Quote\Model\QuoteFactory;

class Products extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * Contact factory
     *
     * @var ContactFactory
     */
    protected $quoteFactory;

    /**
     * @var  \Magento\Framework\Registry
     */
    protected $registry;

    protected $_objectManager = null;

    /**
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Framework\Registry $registry
     * @param ContactFactory $attachmentFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        QuoteFactory $quoteFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        array $data = []
    ) {
        $this->quoteFactory = $quoteFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->_objectManager = $objectManager;
        $this->registry = $registry;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * _construct
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('productsGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        if ($this->getRequest()->getParam('entity_id')) {
            $this->setDefaultFilter(array('in_product' => 1));
        }
    }

    /**
     * add Column Filter To Collection
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_product') {
            $productIds = $this->_getSelectedProducts();

            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in' => $productIds));
            } else {
                if ($productIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', array('nin' => $productIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }

        return $this;
    }

    /**
     * prepare collection
     */
    protected function _prepareCollection()
    {
        $collection = $this->productCollectionFactory->create();
        $collection->addAttributeToSelect('name');
        $collection->addAttributeToSelect('sku');
        $collection->addAttributeToSelect('price');
        $collection->addAttributeToSelect('qty');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        /* @var $model \Webspeaks\ProductsGrid\Model\Slide */
        $model = $this->_objectManager->get('\Magento\Quote\Model\Quote');

        $this->addColumn(
            'in_product',
            [
                'header_css_class' => 'a-center',
                'type' => 'checkbox',
                'field_name' => 'products_id[]',
                'align' => 'center',
                'index' => 'entity_id',
                'values' => $this->_getSelectedProducts(),
            ]
        );

        $this->addColumn(
            'item_id',
            [
                'filter' => false,
                'sortable' => false,
                'header' => __('Item'),
                'renderer' => \Gysa\Quotes\Block\Adminhtml\Quotes\Edit\Tab\Renderer\Item::class,
                'name' => 'item',
                'column_css_class'=>'no-display',
                'header_css_class'=>'no-display',
                'type' => 'hidden',
                'values' => $this->getItemQty(),
                'index' => 'item'
            ]
        );
        $this->addColumn(
            'name',
            [
                'header' => __('Name'),
                'index' => 'name',
                'class' => 'xxx',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'sku',
            [
                'header' => __('Sku'),
                'index' => 'sku',
                'class' => 'xxx',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'price',
            [
                'header' => __('Price'),
                'type' => 'currency',
                'index' => 'price',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'qty',
            [
                'filter' => false,
                'sortable' => false,
                'header' => __('Qty'),
                'renderer' => \Gysa\Quotes\Block\Adminhtml\Quotes\Edit\Tab\Renderer\Qty::class,
                'name' => 'qty',
                'inline_css' => 'qty',
                'type' => 'input',
                'values' => $this->getItemQty(),
                'index' => 'qty'
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl(){
        return $this->getUrl(
            '*/*/productsgrid',
            ['block' => 'products_grid_serializer', '_current' => true, 'collapse' => null]
        );
    }

    /**
     * @param  object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return '';
    }


    protected function getItemQty(){

        $quote = $this->getQuote();
        $allItems = $quote->getAllVisibleItems();
        $itemsId[] = "";

        foreach ($allItems as $item){
            $itemsId[] .= $item->getItemId();
        }

        return $itemsId;

    }

    protected function _getSelectedProducts(){

        $quote = $this->getQuote();
        $allItems = $quote->getAllVisibleItems();
        $products[] = "";

        foreach ($allItems as $item){
            $itemId = $item->getItemId();
            $quoteItem = $this->getItemModel()->load($itemId);
            $product = $quoteItem->getProductId();
            $products[] .= $product;
        }

        return $products;
    }

    public function getSelectedProducts()
    {
        $quote= $this->getQuote();
        $selected = $quote->getProducts($quote);

        if (!is_array($selected)) {
            $selected = [];
        }
        return $selected;
    }

    protected function getQuote()
    {
        $quoteId = $this->getRequest()->getParam('entity_id');
        $quote   = $this->quoteFactory->create();
        if ($quoteId) {
            $quote->load($quoteId);
        }
        return $quote;
    }


    protected function getItemModel(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();//instance of object manager
        $itemModel = $objectManager->create('Magento\Quote\Model\Quote\Item');//Quote item model to load quote item
        return $itemModel;
    }


    public function canShowTab()
    {
        return true;
    }


    public function isHidden()
    {
        return true;
    }
}
