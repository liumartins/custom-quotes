<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 07/03/19
 * Time: 11:15
 */

namespace Gysa\Quotes\Block\Adminhtml\Quotes\Edit;


/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('quote_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Quote Information'));
    }
}
