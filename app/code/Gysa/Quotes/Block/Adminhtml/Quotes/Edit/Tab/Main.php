<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 20/02/19
 * Time: 16:05
 */

namespace Gysa\Quotes\Block\Adminhtml\Quotes\Edit\Tab;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Store\Model\System\Store;
use Magento\Quote\Model\Quote;

class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{

    protected $_systemStore;
    protected  $quoteFactory;


    public function __construct(Context $context, Registry $registry, FormFactory $formFactory, Store $systemStore, Quote $quoteFactory, array $data = [])
    {
        $this->_systemStore = $systemStore;
        $this->quoteFactory = $quoteFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */

    protected function _prepareForm(){

        $id = $this->getRequest()->getParam('entity_id');



        //$baseURL = $this->_storeManager->getStore()->getBaseUrl();
        //$quoteURL =  $baseURL."gysaquotes/quotes/cart/quoteId/".$quoteId ;
        $collection = $this->quoteFactory->load($id);
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('quote_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General Information'), 'class' => 'fieldset-wide']
        );

        $fieldset->addField(
            'entity_id',
            'hidden',
            ['name' => 'quote_id', 'label' => __('Id'), 'title' => __('Id')]
        );

        $fieldset->addField(
            'is_active',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'is_active',
                'required' => true,
                'options' => ['1' => __('Enabled'), '0' => __('Disabled')]
            ]
        );

        $fieldset->addField(
            'customer_firstname',
            'text',
            ['name' => 'customer_firstname', 'label' => __('Name'), 'title' => __('Name')]
        );

        $fieldset->addField(
            'customer_lastname',
            'text',
            ['name' => 'customer_lastname', 'label' => __('Family Name'), 'title' => __('Family Name')]
        );


        $fieldset->addField(
            'customer_email',
            'text',
            ['name' => 'customer_email', 'label' => __('Customer E-mail'), 'title' => __('Customer E-mail'), 'required' => true]
        );

        $fieldset->addField(
            'quote_url',
            'text',
            [
                'name' => 'quote_url',
                'label' => __('Cart URL'),
                'title' => __('Cart URL'),
                'readonly' => true

            ]
        );


        if(!$collection->getId()){
            $collection->setData('is_active', '1');
        }

        $form->setValues($collection->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Main');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Main');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

}