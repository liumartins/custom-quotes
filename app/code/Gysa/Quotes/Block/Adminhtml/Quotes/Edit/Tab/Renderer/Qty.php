<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 11/03/19
 * Time: 11:31
 */

namespace Gysa\Quotes\Block\Adminhtml\Quotes\Edit\Tab\Renderer;

use Magento\Framework\DataObject;
use Magento\Quote\Model\Quote\ItemFactory;
use Magento\Catalog\Model\ProductTypes\ConfigInterface;



class Qty extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Input
{
    protected $itemFactory;
    protected $typeConfig;

    public function __construct(ItemFactory $itemFactory, ConfigInterface $typeConfig)
    {
        $this->itemFactory = $itemFactory;
        $this->typeConfig = $typeConfig;
    }

    protected function _isInactive($row)
    {
        return $this->typeConfig->isProductSet($row->getTypeId());
    }


    public function render(DataObject $row){

        $disabled = '';
        $addClass = '';
        $qty = '';
        $productOnRow = $row->getData("entity_id");
        $itemsId  = $this->getColumn()->getData('values');

        $item  = $this->itemFactory->create();
        if ($this->_isInactive($row)) {
            $qty = '';
            $disabled = 'disabled="disabled" ';
            $addClass = ' input-inactive';
        } else {
            foreach ($itemsId as $itemId) {
                $product = $item->load($itemId)->getProductId();
                if ($product == $productOnRow) {
                    $qty = round($item->load($itemId)->getQty());

                }
            }
        }

        // Compose html
        $html = '<input type="text" ';
        $html .= 'name="' . $this->getColumn()->getId() . '[]" ';
        $html .= 'value="' . $qty . '"' . $disabled;
        $html .= 'class="input-text admin__control-text ' . $this->getColumn()->getInlineCss() . $addClass . '" />';
        return $html;

    }

}