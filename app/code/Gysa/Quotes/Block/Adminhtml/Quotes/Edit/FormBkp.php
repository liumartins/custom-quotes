<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 20/02/19
 * Time: 16:05
 */

namespace Gysa\Quotes\Block\Adminhtml\Quotes\Edit;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Store\Model\System\Store;
use Magento\Quote\Model\Quote;

class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

    protected $_systemStore;
    protected  $quoteFactory;


    public function __construct(Context $context, Registry $registry, FormFactory $formFactory, Store $systemStore, Quote $quoteFactory, array $data = [])
    {
        $this->_systemStore = $systemStore;
        $this->quoteFactory = $quoteFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */

    protected function _construct()
    {
        parent::_construct();
        $this->setId('quote_form');
        $this->setTitle(__('Quote Information'));
    }


    protected function _prepareForm()
    {

        $id = $this->getRequest()->getParam('entity_id');



        //$baseURL = $this->_storeManager->getStore()->getBaseUrl();
        //$quoteURL =  $baseURL."gysaquotes/quotes/cart/quoteId/".$quoteId ;
        $collection = $this->quoteFactory->load($id);
        $form = $this->_formFactory->create(
            [
                'data' =>
                [
                    'id' => 'edit_form',
                    'action' => $this->getData('action'),
                    'method' => 'post'
                ]
            ]
        );

        $form->setHtmlIdPrefix('quote_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General Information'), 'class' => 'fieldset-wide']
        );

        $fieldset->addField(
            'firstname',
            'text',
            ['name' => 'firstname', 'label' => __('Name'), 'title' => __('Name')]
        );

        $fieldset->addField(
            'customer_email',
            'text',
            ['name' => 'customer_email', 'label' => __('Customer E-mail'), 'title' => __('Customer E-mail'), 'required' => true]
        );

        $fieldset->addField(
            'quote_url',
            'text',
            [
                'name' => 'quote_url',
                'label' => __('Cart URL'),
                'title' => __('Cart URL'),
                'readonly' => true

            ]
        );



        $fieldset->addField(
            'is_active',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'is_active',
                'required' => true,
                'options' => ['1' => __('Enabled'), '0' => __('Disabled')]
            ]
        );


        if(!$collection->getId()){
            $collection->setData('is_active', '1');
        }

        $form->setValues($collection->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}