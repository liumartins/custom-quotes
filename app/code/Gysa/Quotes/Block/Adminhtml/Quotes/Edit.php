<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 20/02/19
 * Time: 16:24
 */

namespace Gysa\Quotes\Block\Adminhtml\Quotes;

use Magento\Backend\Block\Widget\Context;
use Magento\Quote\Model\QuoteFactory;


class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     *
     * @var Registry
     */
    protected $quoteFactory;

    /**
     * constructor
     *
     * @param Registry $coreRegistry
     * @param Context $context
     * @param array $data
     */

    public function __construct(
        Context $context,
        QuoteFactory $quoteFactory,
        array $data = []
    )
    {
        $this->quoteFactory = $quoteFactory;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        $this->_objectId   = 'entity_id';
        $this->_blockGroup = 'Gysa_Quotes';
        $this->_controller = 'adminhtml_quotes';

        parent::_construct();

        if($this->_isAllowedAction('Gysa_Quotes::quotes_save'))
        {
            $this->buttonList->update('save', 'label', __('Save Quote'));
            $this->buttonList->add(
                'save-and-continue',
                [
                    'label'          => __('Save and Continue Edit'),
                    'class'          => 'save',
                    'data_attribute' => [
                        'mage-init' => [
                            'button' => [
                                'event'  => 'saveAndContinueEdit',
                                'target' => '#edit_form'
                            ]
                        ]
                    ]
                ],
                -100
            );
        }
        else
        {
            $this->buttonList->remove('save');
        }
    }

    public function getHeaderText()
    {
        $quote = $this->getQuote();
        if ($quote->getId())
        {
            return __("Edit Quote '%1'", $this->escapeHtml($quote->getId()));
        }
    }

    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('quotes/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '']);
    }


    public function getQuote()
    {
        $quote = $this->quoteFactory->create();

        return $quote;
    }
}